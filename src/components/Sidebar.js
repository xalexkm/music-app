import React, { Component } from "react";
import "./sidebar.scss";

class Sidebar extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <div className="msc--sidebar">
          <div className="msc--sidebar--logo">
            <span className="msc--font--primary__regular">Music App</span>
          </div>
          <div className="msc--sidebar--main msc--font--primary__regular">
            <div className="msc--sidebar--item">
              <img src="" alt="" />
              <span className="msc--font--primary__regular">Home</span>
            </div>
          </div>
          <div className="msc--sidebar--personal"></div>
        </div>
      </>
    );
  }
}

export default Sidebar;
