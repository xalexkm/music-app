import React, { Component } from "react";
import "./app.scss";
import Sidebar from "./Sidebar";

class App extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="msc--app">
        <div className="msc--container">
          <div className="msc--main">
            <Sidebar></Sidebar>
            <div className="msc--dashboard"></div>
          </div>
          <div className="msc--player"></div>
        </div>
      </div>
    );
  }
}

export default App;
